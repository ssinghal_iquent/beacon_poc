/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {startProximityObserver} from './estimoteObserver';

AppRegistry.registerComponent(appName, () => App);

startProximityObserver();
