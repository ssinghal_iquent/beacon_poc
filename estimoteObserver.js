// @flow
"use strict";

import * as RNEP from "@estimote/react-native-proximity";
import { ApiHelperPOST } from "./APIhelper";


const startProximityObserver = () => {

  // generate Estimote Cloud credentials for your app at:
  // https://cloud.estimote.com/#/apps/add/your-own-app
  const ESTIMOTE_APP_ID = "foodies-beacon-testdemo-g58";
  const ESTIMOTE_APP_TOKEN = "09b02ee4320260917ca79f8d635834af";

  // will trigger when the user is within ~ 5 m of any beacon with tag "lobby"
  // you can add tags to your beacons on https://cloud.estimote.com, in Beacon Settings
  // will trigger when the user is within ~ 5 m of any beacon with tag "lobby"
    // you can add tags to your beacons on https://cloud.estimote.com, in Beacon Settings
    const zone1 = new RNEP.ProximityZone(10, 'ghana_event');
    console.log('zone1', zone1);
    ApiHelperPOST('https://dummy-logs-app.herokuapp.com/logs', zone1);
    zone1.onEnterAction = context => {
      console.log("zone1 onEnter", context);
      Alert.alert(JSON.stringify(context));
      ApiHelperPOST('https://dummy-logs-app.herokuapp.com/logs', context);
    };
    zone1.onExitAction = context => {
      console.log("zone1 onExit", context);
      Alert.alert(JSON.stringify(context));
      ApiHelperPOST('https://dummy-logs-app.herokuapp.com/logs', context);
    };
    zone1.onChangeAction = contexts => {
      console.log("zone1 onChange", contexts);
      Alert.alert(JSON.stringify(contexts));
      ApiHelperPOST('https://dummy-logs-app.herokuapp.com/logs', contexts);
    };

  // detecting proximity to Bluetooth beacons gives you information about the user's location, and so
  // on both iOS and Android it's required to ask the user for permission to do that
  //
  // - on iOS, the user can choose between "never", "only when using the app" and "always" (background)
  //   - however, background support also requires that you enable the "Uses Bluetooth LE accessories"
  //     Background Mode for your app
  //   - you can do that in Xcode project settings, on the Capabilities tab
  //   - you might also need to explain/defend your app's background usage during the App Store review
  //
  // - on Android, it'll be a simple "yes/no" popup, which is equivalent to "never" and "always"
  //   - however, to have it work in the background, you're also required to show a notification, so
  //     that the user knows that the app keeps running/detecting beacons even if they close it
  //   - see the `config` section below for how to enable/configure such notification
  //
  // see also: "Location permission" and "Background support" sections in the README
  RNEP.locationPermission.request().then(
    permission => {
      // `permission` will be one of RNEP.locationPermission.DENIED, .ALWAYS, or .WHEN_IN_USE
      console.log(`location permission: ${permission}`);

      if (permission !== RNEP.locationPermission.DENIED) {
        const credentials = new RNEP.CloudCredentials(
          ESTIMOTE_APP_ID,
          ESTIMOTE_APP_TOKEN
        );

        const config = {
          // modern versions of Android require a notification informing the user that the app is active in the background
          // if you don't need proximity observation to work in the background, you can omit the entire `notification` config
          //
          // see also: "Background support" section in the README
          notification: {
            title: "Exploration mode is on",
            text: "We'll notify you when you're next to something interesting.",
            //icon: 'my_drawable', // if omitted, will default to the app icon (i.e., mipmap/ic_launcher)

            // in apps targeting Android API 26, notifications must specify a channel
            // https://developer.android.com/guide/topics/ui/notifiers/notifications#ManageChannels
            channel: {
              id: "exploration-mode",
              name: "Exploration Mode"
            }
          }
        };

        RNEP.proximityObserver.initialize(credentials, config);
        RNEP.proximityObserver.startObservingZones([zone1]);
      }
    },
    error => {
      console.error("Error when trying to obtain location permission", error);
    }
  );

};

const stopProximityObserver = () => { RNEP.proximityObserver.stopObservingZones(); };

export {startProximityObserver, stopProximityObserver};